using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(!other.name.Contains("Player")) return;

        GameManager.DamagePlayer(10);
        Destroy(this.gameObject);
    }
}
