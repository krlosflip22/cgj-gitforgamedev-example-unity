﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PlayerMode
{
    Attack,
    Idle
}

public class GameManager : MonoBehaviour
{
    int totalScore = 0;
    int totalLife = 100;

    int totalTime = 10;

    [SerializeField] Text ScoreText;
    [SerializeField] Text LifeText;

    [SerializeField] Text TimeText;

    [SerializeField] Material attackMaterial;
    [SerializeField] Material idleMaterial;
    [SerializeField] Renderer playerRenderer;

    PlayerMode playerMode = PlayerMode.Idle;
    public static PlayerMode PlayerMode { get => instance.playerMode; }

    private static GameManager instance;

    public static void IncreaseScore()
    {
        instance.totalScore += 10;
        instance.ScoreText.text = "Score: " + instance.totalScore;
    }

    public static void DamagePlayer(int damage)
    {
        instance.totalLife -= damage;
        instance.LifeText.text = "Life: " + instance.totalLife;
    }

    public static void ResetAll()
    {
        instance.totalScore = 0;
        instance.totalLife = 100;
        instance.totalTime = 10;
        instance.LifeText.text = "Life: " + instance.totalLife;
        instance.ScoreText.text = "Score: " + instance.totalScore;
        instance.TimeText.text = "Time: " + string.Format("{00}", instance.totalTime.ToString());
    }

    public static void StartCountdown()
    {
        instance.startCountdown();
    }
    private void startCountdown()
    {
        StartCoroutine(CountDownRoutine());
    }

    IEnumerator CountDownRoutine()
    {
        while(totalTime > 0)
        {
            yield return new WaitForSeconds(1);

            totalTime--;
            TimeText.text = "Time: " + string.Format("{00}", totalTime.ToString());
        }

        UIManager.ShowFinalPanel(totalScore);
    }

    private void Awake()
    {
        instance = this;
    }

    // private void Start()
    // {
    //     StartCoroutine(CountDownRoutine());
    // }

    void Update()
    {
        // if(Input.GetKeyDown(KeyCode.Alpha1))
        // {
        //     playerMode = PlayerMode.Idle;
        //     playerRenderer.material = idleMaterial;
        // }

        // if(Input.GetKeyDown(KeyCode.Alpha2))
        // {
        //     playerMode = PlayerMode.Attack;
        //     playerRenderer.material = attackMaterial;
        // }
    }

}
