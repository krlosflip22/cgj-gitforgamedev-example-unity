﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectGenerator : MonoBehaviour
{
    [SerializeField] GameObject[] objectPrefabs;

    void Start()
    {
        for(int i = 0; i < 20; i++)
        {
            Vector3 positionOffset = new Vector3(GetRandomRange(), 0, GetRandomRange());
            Instantiate(objectPrefabs[Random.Range(0, objectPrefabs.Length)], transform.position + positionOffset, Quaternion.identity);
        }
    }

    float GetRandomRange()
    {
        float min = -25;
        float max = 25;
        float minExclusive = -2;
        float maxExclusive = 2;

        var excluded = maxExclusive - minExclusive;
        var newMax = max - excluded;
        var outcome = Random.Range(min, newMax);

        return outcome > minExclusive ? outcome + excluded : outcome;
    }

}
