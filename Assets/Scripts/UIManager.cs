﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIManager : MonoBehaviour
{
    [SerializeField] GameObject[] menuPanels;
    [SerializeField] Text scoreText;

    private static UIManager instance;

    private void Awake()
    {
        instance = this;
    }

    public static void ShowFinalPanel(int score)
    {
        instance.menuPanels[2].SetActive(true);
        instance.scoreText.text = $"Score: {score}";
    }

    public void OnMenuButtonPressed(int index)
    {
        switch(index)
        {
            case 0:
                    menuPanels[0].SetActive(false);
                    GameManager.StartCountdown();
                    break;
            case 1:
                    Application.Quit();
                    break;
            case 2:
                    menuPanels[1].SetActive(false);
                    break;
            case 3:
            case 4:
                    menuPanels[2].SetActive(false);
                    menuPanels[0].SetActive(true);
                    GameManager.ResetAll();
                    break;
        }
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            menuPanels[1].SetActive(true);
        }

        if(Input.GetKeyDown(KeyCode.R))
        {
            menuPanels[0].SetActive(true);
        }
    }
}
